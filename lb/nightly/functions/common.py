###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from datetime import datetime
from time import time
import os
import socket
import json
from collections import namedtuple
import logging
from subprocess import CalledProcessError, run
from git import GitCommandError
from zlib import decompress
from shutil import copyfileobj
from tempfile import NamedTemporaryFile
from lb.nightly.configuration import Project, Package, DataProject


class Report:
    """
    Class used to collect reports.

    Record messages and forward them to a logger.

    Apart from normal logging levels, accept extra type of messages, like
    ``git_error``.
    """

    def __init__(self, logger=__name__):
        self.logger = logging.getLogger(logger)
        self.records = []

    def log(self, type, level, fmt, args=None):
        record = {"type": type, "level": level, "text": (fmt % args if args else fmt)}
        if type in ("error", "warning") and f"{type}:" not in record["text"]:
            record["text"] = "{}: {}".format(type, record["text"])
        self.records.append(record)
        getattr(self.logger, level)(fmt, *(() if args is None else args))

    def debug(self, fmt, *args):
        self.log("debug", "debug", fmt, args)

    def info(self, fmt, *args):
        self.log("info", "info", fmt, args)

    def warning(self, fmt, *args):
        self.log("warning", "warning", fmt, args)

    warn = warning

    def error(self, fmt, *args):
        self.log("error", "error", fmt, args)

    def git_error(self, msg, err: GitCommandError):
        """
        Special handling for GitCommandError
        """
        self.warning("%s: GitCommand status %s", msg, err.status)
        # FIXME: this is very much subject to the internal details of GitCommandError
        self.log("command", "debug", "> %s", (err._cmdline,))
        if "'" in err.stdout:
            self.log(
                "stdout", "debug", err.stdout[err.stdout.index("'") + 1 : -1].rstrip()
            )
        if "'" in err.stderr:
            self.log(
                "stderr", "debug", err.stderr[err.stderr.index("'") + 1 : -1].rstrip()
            )

    def md(self):
        """
        Format as markdown.
        """
        out = []
        if hasattr(self, "project"):
            out.append("# {name}/{version}".format(**self.project))
        for r in self.records:
            lines = r["text"].splitlines()
            if r["type"] in ("stdout", "stderr", "command"):
                out.append("  ```")
                out.extend("  " + l for l in lines)
                out.append("  ```")
            else:
                out.append("- " + lines.pop(0))
                out.extend("  " + l for l in lines)
        if hasattr(self, "packages"):
            out.extend(pkg.md() for pkg in self.packages)
        return "\n".join(out)

    def __str__(self):
        out = []
        if hasattr(self, "project"):
            out.append("{name}/{version}".format(**self.project))
            out.append("-" * len(out[-1]))
            out.append("")
        out.extend(r["text"] for r in self.records)
        if hasattr(self, "packages"):
            out.extend(str(pkg) for pkg in self.packages)
        return "\n".join(out)

    def to_dict(self):
        from copy import deepcopy

        data = deepcopy(self.__dict__)
        data["logger"] = self.logger.name
        if "packages" in data:
            data["packages"] = [pkg.to_dict() for pkg in data["packages"]]

        return data


def ensure_dir(path, rep: Report):
    """
    Make sure that a directory exist, creating it
    and passing a warning to Report object if dir does not exist.
    """
    if not os.path.exists(path):
        rep.warning(f"directory {path} is missing, I create it")
        os.makedirs(path)


def find_path(name, search_path=None):
    """
    Look for a file or directory in a search path.

    If the search path is not specified, the concatenation of CMTPROJECTPATH
    and CMAKE_PREFIX_PATH is used.

    >>> find_path('true', ['/usr/local/bin', '/bin'])
    '/bin/true'
    >>> print(find_path('cannot_find_me', []))
    None
    """
    from os import environ, pathsep
    from os.path import join, exists

    if search_path is None:
        search_path = environ.get("CMAKE_PREFIX_PATH", "").split(pathsep) + environ.get(
            "CMTPROJECTPATH", ""
        ).split(pathsep)

    try:
        return next(
            join(path, name) for path in search_path if exists(join(path, name))
        )
    except StopIteration:
        logging.warning("%s not found in %r", name, search_path)
    return None


def get_build_method(project=None):
    """
    Helper function to get a build method for a project.

    The method is looked up in the following order: build_tool property of a project,
    build_tool property of a slot owning the project, the default build method (cmake).

    If a method is retrieved via build_tool string property, it must be defined in
    lb.nightly.functions.build.
    """
    import lb.nightly.functions.build as build_methods

    try:
        method = project.build_tool or project.slot.build_tool
        return getattr(build_methods, method)
    except (AttributeError, TypeError):
        return build_methods.cmake


def safe_dict(mydict):
    """Helper to return the dictionary without sensitive data
    To be used e.g. to remove secret environment variables.
    >>> d={"PASSWORD": "my_secret_pass", "PASS": "asd", "USER": "me", "KEY": "asd", "TOKEN": "asd", "PRIVATE": "Asd", "HOME": "Asd", "SHA": "asd"}
    >>> safe_dict(d)
    {}
    """
    return {
        k: v
        for k, v in mydict.items()
        if all(
            s not in k.upper()
            for s in [
                "PASS",
                "USER",
                "KEY",
                "TOKEN",
                "PRIVATE",
                "HOME",
                "SHA",
            ]
        )
    }


cernvm = {
    "slc5": "/cvmfs/cernvm-prod.cern.ch/cvm3",
    "slc6": "/cvmfs/cernvm-prod.cern.ch/cvm4",
    "centos7": "/cvmfs/cernvm-prod.cern.ch/cvm4",
}


def singularity_run(cmd, env={}, cwd=os.getcwd(), task={}, collect=False):
    from spython.main import Client

    try:
        os_id = env["BINARY_TAG"].split("-")[1]
        Client.load(cernvm[os_id])
    except KeyError:
        raise EnvironmentError(
            f"Could not find the CernVM image for the "
            f"{env.get('BINARY_TAG', 'not specified BINARY_TAG')}"
        )

    s_env = {
        k: env.get(k)
        for k in ("PATH", "CMAKE_PREFIX_PATH", "BINARY_TAG", "CMTCONFIG", "LCG_VERSION")
        if k in env
    }
    s_env.update(env)
    s_env = safe_dict(s_env)

    # setting up elasticsearch where we send logs from singularity
    from lb.nightly.configuration import service_config
    from elasticsearch import Elasticsearch, helpers

    conf = service_config()
    es_url = conf.get("elasticsearch", {}).get("uri")
    elastic = Elasticsearch([es_url])
    logging.getLogger("elasticsearch").setLevel(logging.WARNING)
    if elastic.ping():
        # stub of the msg to sent to elasticsearch
        log_body = {
            "host": socket.gethostname(),
            "command": cmd,
            "cwd": cwd,
            "task": task.get("task"),
            "project": task.get("project"),
            "platform": task.get("platform"),
            "worker_task_id": task.get("worker_task_id"),
            "log": None,  # will be overriden just before sending
            "timestamp": None,  # same as above
        }
        log_body.update(
            {
                "log": f"Running command: {cmd} with env: {s_env}",
                "timestamp": datetime.now(),
            }
        )
        elastic.index(index=f"logs-{task.get('task', 'nightlies')}", body=log_body)
    if "TASK_LOGFILE" in env:
        with open(env.get("TASK_LOGFILE"), "a") as logfile:
            logfile.write(
                f"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}: "
                f"Running command: {cmd} with {s_env}\n"
            )
    if cwd != os.getcwd():
        cwd = os.path.join(os.getcwd(), cwd)

    # to collect the build messages we communicate through the named pipe
    if collect:
        from hashlib import sha256

        hash = sha256()
        hash.update(f"{cmd} {task} {cwd}".encode())
        os.environ["LB_WRAPCMD_SOCKET"] = s_env["LB_WRAPCMD_SOCKET"] = os.path.join(
            cwd, hash.hexdigest()[:32]
        )

    Result = namedtuple("Result", ["returncode", "stdout", "stderr", "args"])
    try:
        result = Client.execute(
            cmd,
            bind=["/cvmfs", env.get("CONDA_ENV"), cwd],
            options=["--writable-tmpfs", f"--pwd={cwd}", "--cleanenv"]
            + [f"--env={k}={v}" for k, v in s_env.items()],
            stream=True,
        )

        def read_from_singularity(result, sock=None):
            stdout = ""

            def get_stdout(result):
                nonlocal stdout
                for line in result:
                    stdout += line
                    if sock:
                        try:
                            conn, _ = sock.accept()
                        except socket.timeout:
                            continue
                        with conn:
                            buffer = b""
                            while True:
                                msg = conn.recv(4096)
                                if msg:
                                    buffer += msg
                                else:
                                    data = json.loads(decompress(buffer))
                                    with open(data.pop("id"), "w") as of:
                                        of.write("\n".join(data.values()))
                                    break
                    yield line

            def log_to_file(result):
                with open(
                    os.environ.get("TASK_LOGFILE"),
                    "a",
                    buffering=512,
                ) as logfile:
                    for line in result:
                        logfile.write(
                            f"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}: "
                            f"{line.rstrip()}\n"
                        )
                        yield line

            def log_to_elastic(result):
                for line in result:
                    yield dict(
                        log_body,
                        **{
                            "_index": f"logs-{task.get('task', 'nightlies')}",
                            "log": line.rstrip(),
                            "timestamp": datetime.now(),
                        },
                    )

            if "TASK_LOGFILE" in env:

                def get_output_and_log(result):
                    if elastic.ping():
                        return helpers.streaming_bulk(
                            elastic, log_to_elastic(log_to_file(get_stdout(result)))
                        )
                    else:
                        return log_to_file(get_stdout(result))

            else:

                def get_output_and_log(result):
                    if elastic.ping():
                        return helpers.streaming_bulk(
                            elastic, log_to_elastic(get_stdout(result))
                        )
                    else:
                        return get_stdout(result)

            for _ in get_output_and_log(result):
                pass

            return stdout

        if collect:
            server_address = os.environ["LB_WRAPCMD_SOCKET"]
            try:
                os.unlink(server_address)
            except OSError:
                if os.path.exists(server_address):
                    raise
            with socket.socket(socket.AF_UNIX, socket.SOCK_STREAM) as sock:
                sock.bind(server_address)
                sock.listen()
                sock.settimeout(1)
                stdout = read_from_singularity(result, sock)
        else:
            if "LB_WRAPCMD_SOCKET" in os.environ:
                del os.environ["LB_WRAPCMD_SOCKET"]
            stdout = read_from_singularity(result)
        return Result(stdout=stdout, returncode=0, stderr=b"", args=cmd)

    except CalledProcessError as cpe:
        return Result(
            stdout=cpe.stdout,
            returncode=cpe.returncode,
            stderr=cpe.stderr,
            args=cmd,
        )


def lb_wrapcmd():
    """
    The script to be used in CMake launcher rules.

    A launcher rule defined as
    set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE "lb-wrapcmd <CMAKE_CURRENT_BINARY_DIR> <TARGET_NAME>")

    Output for each compile command is sent to the socket
    (name taken from LB_WRAPCMD_SOCKET env variable or built as:
    <CMAKE_CURRENT_BINARY_DIR>/1234-<TARGET_NAME>-abc-build.log)
    """
    from time import time_ns
    from argparse import ArgumentParser, REMAINDER
    from hashlib import sha256
    from zlib import compress

    parser = ArgumentParser()
    parser.add_argument("log_dir", help="CMAKE_CURRENT_BINARY_DIR")
    parser.add_argument("target", help="TARGET_NAME")
    parser.add_argument("wrap_cmd", nargs=REMAINDER, help="wrapped command")
    args = parser.parse_args()

    result = run(args.wrap_cmd, capture_output=True)
    if "LB_WRAPCMD_SOCKET" in os.environ:
        server_address = os.environ["LB_WRAPCMD_SOCKET"]

        hash = sha256()
        hash.update(f"{args.wrap_cmd} {args.log_dir} {args.target}".encode())

        output = {
            "id": f"{args.log_dir}/{time_ns()}-{args.target}-{hash.hexdigest()[:32]}-build.log",
            "cmd": f"""\033[0;32m({args.log_dir})$ {' '.join(args.wrap_cmd)}\033[0m""",
            "stdout": None,
            "stderr": None,
        }

        if args.wrap_cmd[0] != "cat":
            output["stdout"] = result.stdout.decode()
            output["stderr"] = result.stderr.decode()
        if result.returncode:
            output[
                "stderr"
            ] += f"""\n\033[0;31m[command exited with {result.returncode}]\033[0m"""
        with socket.socket(socket.AF_UNIX, socket.SOCK_STREAM) as sock:
            sock.settimeout(1)
            sock.connect(server_address)
            to_sent = compress(json.dumps(output).encode())
            if len(to_sent) < sock.getsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF):
                sock.sendall(to_sent)
            else:
                print(
                    f"Could not sent the data because its size ({len(to_sent)} bytes) exceeds the socket buffer size"
                )
                return 1
    return result.returncode


def download_and_unzip(repo, artifact):
    """
    Helper function to download the artifact
    and unzip it in the current directory.

    :param repo: ArtifactsRepository object to get the artifact from
    :param artifact: the value returned by the `artifacts`
    method of the Project.

    Returns True if succeded.
    """
    try:
        with NamedTemporaryFile() as lp:
            with repo.pull(artifact) as remote:
                copyfileobj(remote, lp)
            lp.seek(0)
            return not run(["unzip", "-o", lp.name], check=True).returncode
    except Exception as err:
        return False


def get_artifacts_list(project, platform, stage="build"):
    """
    Helper function returning the list of the artifcts
    needed to perform `stage` (build or test) for `project` and `platform`
    """
    artifacts = []
    if stage in ["build", "test"]:
        artifacts.append(project.artifacts("checkout"))
        if stage == "test":
            artifacts.append(project.artifacts("build", platform))
        for dep_name in project.dependencies():
            if dep_name in project.slot.projects:
                dependency = project.slot.projects[dep_name]
                if isinstance(dependency, (Package, DataProject)):
                    artifacts.append(dependency.artifacts("checkout"))
                elif isinstance(dependency, Project):
                    artifacts.append(dependency.artifacts("build", platform))
    return artifacts
