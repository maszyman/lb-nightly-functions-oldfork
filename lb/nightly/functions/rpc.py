###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Entry point functions for nightly build operations.
"""
import os
import pathlib
import json
from subprocess import check_output, STDOUT, run
from multiprocessing import Pool
from lb.nightly.configuration import get, Project, Package, DataProject, service_config
from lb.nightly.utils import Repository
from lb.nightly.db import connect as db
from .common import Report, singularity_run, download_and_unzip, get_artifacts_list


def checkout(project_id: str, worker_task_id: str, gitlab_token=None):
    from .checkout import git, notify_gitlab, update_nightly_git_archive

    conf = service_config()

    gitlab_token = (
        gitlab_token
        or conf.get("gitlab", {}).get("token")
        or os.environ.get("GITLAB_TOKEN")
    )

    project = get(project_id)
    if not isinstance(project, (Project, Package)):
        raise ValueError(
            f"project_id {project_id} does not identify a nightly builds Project instance"
        )

    db().checkout_start(project, worker_task_id)

    # checkout the project
    report = git(project)

    # record the working directory
    report.cwd = os.getcwd()

    if gitlab_token:
        if isinstance(project, DataProject):
            for pkg, pkg_report in zip(project.packages, report.packages):
                notify_gitlab(pkg, pkg_report, gitlab_token)
        else:
            report = notify_gitlab(project, report, gitlab_token)

    # gitlab archive and dependencies are not possible for data projects
    if not isinstance(project, (DataProject, Package)):
        try:
            report = update_nightly_git_archive(project, report)
        except Exception as err:
            report.warning(f"failed to update Git archive: {err}")

        # resolve and update dependencies
        db().set_dependencies(project)

    if isinstance(project, Package):
        dir_to_pack = os.path.join(
            project.container.name, project.name, project.version
        )
    else:
        dir_to_pack = project.name
    archive_name = os.path.basename(project.artifacts("checkout"))

    report.info(f"packing {dir_to_pack} into {archive_name}")
    report.log(
        "stdout",
        "debug",
        check_output(
            [
                "zip",
                "-r",
                archive_name,
                dir_to_pack,
            ],
            stderr=STDOUT,
        ).decode(),
    )

    report.info(f"adding logs to {archive_name}")

    log_dir = pathlib.Path(project.name) / ".logs" / "checkout"
    os.makedirs(log_dir)
    with open(log_dir / "report.json", "w") as f:
        json.dump(report.to_dict(), f, indent=2)
    with open(log_dir / "report.md", "w") as f:
        f.write(report.md())

    report.log(
        "stdout",
        "debug",
        check_output(
            ["zip", "-r", archive_name, log_dir],
            stderr=STDOUT,
        ).decode(),
    )

    if "artifacts" in conf:
        artifacts_repo = Repository.connect(conf["artifacts"]["uri"])
        assert artifacts_repo.push(
            open(archive_name, "rb"), project.artifacts("checkout")
        ), "failed to upload artifacts"
    else:
        report.warning("artifacts repository not configured: no publishing")

    if "logs" in conf:
        logs_repo = Repository.connect(conf["logs"]["uri"])
        assert logs_repo.push(
            open(log_dir / "report.json", "rb"),
            project.artifacts("checkout") + "-report.json",
        ), "failed to upload log"

    else:
        report.warning("logs repository not configured: no publishing")

    db().checkout_complete(project, report, worker_task_id)

    return report


def build(
    project_id: str,
    platform: str,
    worker_task_id: str,
    gitlab_token=None,
):

    conf = service_config()
    repo = Repository.connect()

    project = get(project_id)
    if not isinstance(project, Project):
        raise ValueError(
            f"project_id {project_id} does not identify a nightly builds Project instance"
        )

    db().build_start(project, platform, worker_task_id)

    with Pool() as pool:
        tasks = pool.starmap(
            download_and_unzip,
            [(repo, art) for art in get_artifacts_list(project, platform, "build")],
        )

    build_env = {}
    build_env["BINARY_TAG"] = platform
    host_os = singularity_run(
        ["/cvmfs/lhcb.cern.ch/lib/bin/host_os"], env=build_env
    ).stdout.rstrip()
    # we need lb-project-init from LbEnv
    build_env[
        "PATH"
    ] = f"/cvmfs/lhcb.cern.ch/lib/bin/{host_os}:{os.environ['PATH']}:/cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/1012/stable/linux-64/bin"
    if project.build_tool == "cmt" or project.slot.build_tool == "cmt":
        os.environ[
            "PATH"
        ] = f"/cvmfs/lhcb.cern.ch/lib/bin/Linux-x86_64:{os.environ['PATH']}"
    build_env[
        "CMAKE_PREFIX_PATH"
    ] = f"{os.getcwd()}:{os.getcwd()}/Gaudi/cmake:/cvmfs/lhcb.cern.ch/lib/lhcb:/cvmfs/lhcb.cern.ch/lib/lcg/releases"
    if "TASK_LOGFILE" in os.environ:
        build_env["TASK_LOGFILE"] = os.environ["TASK_LOGFILE"]
    if "CONDA_ENV" in os.environ:
        build_env["CONDA_ENV"] = os.environ["CONDA_ENV"]

    from lb.nightly.functions.common import get_build_method

    report = get_build_method(project)(project).build(
        jobs=int(os.environ.get("LBN_BUILD_JOBS") or "0") or os.cpu_count() or 1,
        env=build_env,
        worker_task_id=worker_task_id,
    )

    archive_name = os.path.basename(project.artifacts("build", platform))
    report["script"].info(f"packing {project.name} into {archive_name}")
    report["script"].log(
        "stdout",
        "debug",
        check_output(
            [
                "zip",
                "-r",
                archive_name,
                project.name,
                "-i",
                f"{project.name}/build/*",
                f"{project.name}/InstallArea/*",
            ],
            stderr=STDOUT,
        ).decode(),
    )

    report["script"].info(f"adding logs to {archive_name}")

    log_dir = pathlib.Path(project.name) / ".logs" / platform / "build"
    os.makedirs(log_dir, exist_ok=True)
    for step in report.keys():
        with open(log_dir / "report.md", "a") as f:
            f.write(f"{report[step].md()}\n")
    with open(log_dir / "report.json", "w") as f:
        json.dump({k: v.to_dict() for k, v in report.items()}, f, indent=2)

    report["script"].log(
        "stdout",
        "debug",
        check_output(["zip", "-r", archive_name, log_dir], stderr=STDOUT).decode(),
    )

    assert repo.push(
        open(archive_name, "rb"), project.artifacts("build", platform)
    ), "failed to upload artifacts"

    if "logs" in conf:
        logs_repo = Repository.connect(conf["logs"]["uri"])
        assert logs_repo.push(
            open(log_dir / "report.json", "rb"),
            project.artifacts("build", platform) + "-report.json",
        ), "failed to upload log"

    else:
        report["script"].warning("logs repository not configured: no publishing")

    db().build_complete(project, platform, report, worker_task_id)

    return report


if __name__ == "__main__":  # pragma: no cover
    import sys
    import logging

    logging.basicConfig(level=logging.DEBUG)
    globals()[sys.argv[1]](*sys.argv[2:])
