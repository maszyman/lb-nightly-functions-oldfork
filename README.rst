=============================
LHCb Nightly Builds Functions
=============================

This project contains the functions used to checkout, build and test projects in
the LHCb Nightly Build and Continuous Integration System.
