###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest
import os
import logging
import zipfile
import elasticsearch
from unittest.mock import patch, MagicMock, Mock
from io import BytesIO
from lb.nightly.functions.rpc import checkout, build
from lb.nightly.configuration import Slot, Project
from lb.nightly.functions.common import get_build_method

from .test_checkout import working_directory


@patch(
    "elasticsearch.Elasticsearch",
)
@patch(
    "shutil.copyfileobj",
)
@patch(
    "lb.nightly.functions.build.run",
)
@patch(
    "lb.nightly.functions.common.run",
)
@patch(
    "lb.nightly.functions.rpc.singularity_run",
)
@patch(
    "lb.nightly.utils.Repository.FileRepository.pull",
)
@patch(
    "lb.nightly.functions.rpc.service_config",
    return_value={"gitlab": {"token": "some_token"}, "artifacts": {"uri": "artifacts"}},
)
@patch(
    "lb.nightly.configuration.service_config",
    return_value={"gitlab": {"token": "some_token"}, "artifacts": {"uri": "artifacts"}},
)
@patch(
    "lb.nightly.functions.rpc.get",
    return_value=Slot(
        "test-slot", build_id=123, projects=[Project("Moore", "HEAD")]
    ).Moore,
)
@patch("lb.nightly.functions.rpc.db")
def test_build_full(
    db,
    get,
    service_config_main,
    service_config,
    pull,
    singularity_run,
    run,
    buildrun,
    copy,
    elastic,
    tmp_path,
):
    copy = MagicMock()
    pull.return_value = BytesIO()
    singularity_run.return_value.returncode = 0
    run.return_value.returncode = 0
    buildrun.return_value.returncode = 0
    platform = "x86_64-centos7-gcc9-opt"
    elastic.ping = Mock(return_value=False)
    with patch("json.dump"), working_directory(tmp_path):
        report = build("flavour/slot/123/project", platform, "")

    service_config.assert_called_once()
    service_config_main.assert_called()
    get.assert_called_once()
    db.assert_has_calls([db(), db()])
    singularity_run.assert_called_once()
    buildrun.assert_called()

    archive_path = tmp_path / os.path.basename(get().artifacts("build", platform))
    assert zipfile.is_zipfile(archive_path)
    with zipfile.ZipFile(archive_path) as archive:
        names = archive.namelist()
        assert f"Moore/.logs/{platform}/build/report.json" in names
        assert f"Moore/.logs/{platform}/build/report.md" in names
    artifact_path = tmp_path / "artifacts" / get().artifacts("build", platform)
    assert os.path.exists(artifact_path)
    assert open(archive_path, "rb").read() == open(artifact_path, "rb").read()
