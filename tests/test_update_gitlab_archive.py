###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from lb.nightly.configuration import Project, Slot
from lb.nightly.functions.common import Report
from unittest.mock import patch, call, Mock, MagicMock
from git import GitCommandError
import pytest


from lb.nightly.functions.checkout import update_nightly_git_archive


def test_no_slot():
    with pytest.raises(AssertionError):
        update_nightly_git_archive(Project("TheProject", "master"), Report())

    slot = Slot("test-slot", projects=[Project("TheProject", "master")])
    with pytest.raises(AssertionError):
        update_nightly_git_archive(slot.TheProject, Report())


@patch("lb.nightly.functions.gitlab_helpers.gitlabProjectExists", return_value=False)
def test_no_mirror(mock):
    slot = Slot("test-slot", projects=[Project("TheProject", "master")])
    slot.build_id = 100

    report = update_nightly_git_archive(slot.TheProject, Report())

    mock.called_once_with("lhcb/TheProject", report)
    assert "TheProject not found on" in str(report)


def setup_mock_for_archive(mock):
    slot = Slot("test-slot", projects=[Project("TheProject", "master")])
    slot.build_id = 100

    report = Report()
    report.tree = "1234abcd"
    report.commit = "fedc0987"

    tagMocks = {}

    def create_tag(name, commit="HEAD"):
        if name not in tagMocks:
            tagMocks[name] = MagicMock(name=name, commit=MagicMock())
        # print(name, "->", tagMocks[name])
        return tagMocks[name]

    class Fetch:
        def __init__(self):
            self.bad_names = []

        def __call__(self, name):
            if name in self.bad_names:
                raise GitCommandError(
                    command=["git", "fetch", "<remote>", name], status=128
                )
            return MagicMock(name=name)

    mock().create_remote().url = "<remote url>"
    mock().create_remote().fetch.side_effect = Fetch()
    mock().create_tag.side_effect = create_tag
    mock().create_tag("test-slot/99").commit.tree.hexsha = "something"
    mock().head.commit.tree.hexsha = report.tree
    mock().git.diff.return_value = "short\vvery_long"
    mock().git.show_branch.return_value = "report show-branch changes"
    mock.reset_mock()

    return slot.TheProject, report, tagMocks


@patch("lb.nightly.functions.gitlab_helpers.gitlabProjectExists", return_value=True)
@patch("lb.nightly.functions.checkout.Repo")
def test_basic(repoMock, _):
    project, report, _ = setup_mock_for_archive(repoMock)

    try:
        report = update_nightly_git_archive(project, report)
    finally:
        print(repoMock.mock_calls)

    assert "changes detected wrt" in str(report)


@patch("lb.nightly.functions.gitlab_helpers.gitlabProjectExists", return_value=True)
@patch("lb.nightly.functions.checkout.Repo")
def test_no_previous_tag(repoMock, _):
    project, report, _ = setup_mock_for_archive(repoMock)

    # no previous tag
    repoMock().create_remote().fetch.side_effect.bad_names = ["test-slot/99"]
    repoMock.reset_mock()

    try:
        report = update_nightly_git_archive(project, report)
    finally:
        print(repoMock.mock_calls)

    assert "no previous tag found" in str(report)


@patch("lb.nightly.functions.gitlab_helpers.gitlabProjectExists", return_value=True)
@patch("lb.nightly.functions.checkout.Repo")
def test_no_branch(repoMock, _):
    project, report, _ = setup_mock_for_archive(repoMock)

    # no branch
    repoMock().create_remote().fetch.side_effect.bad_names = ["test-slot"]
    repoMock.reset_mock()

    try:
        report = update_nightly_git_archive(project, report)
    finally:
        print(repoMock.mock_calls)

    assert "failed to fetch branch" in str(report)
    assert "changes detected wrt" in str(report)


@patch("lb.nightly.functions.gitlab_helpers.gitlabProjectExists", return_value=True)
@patch("lb.nightly.functions.checkout.Repo")
def test_no_branch_no_previous(repoMock, _):
    project, report, _ = setup_mock_for_archive(repoMock)

    # no previous tag and no branch
    repoMock().create_remote().fetch.side_effect.bad_names = [
        "test-slot/99",
        "test-slot",
    ]
    repoMock.reset_mock()

    try:
        report = update_nightly_git_archive(project, report)
    finally:
        print(repoMock.mock_calls)

    assert "failed to fetch branch" in str(report)
    assert "no previous tag found" in str(report)


@patch("lb.nightly.functions.gitlab_helpers.gitlabProjectExists", return_value=True)
@patch("lb.nightly.functions.checkout.Repo")
def test_no_change_with_previous(repoMock, _):
    project, report, _ = setup_mock_for_archive(repoMock)

    # previous tag with same data
    repoMock().create_tag("test-slot/99").commit.tree.hexsha = report.tree
    repoMock.reset_mock()

    try:
        report = update_nightly_git_archive(project, report)
    finally:
        print(repoMock.mock_calls)

    repoMock().head.reset.assert_any_call(repoMock().create_tag("test-slot/99").commit)
    assert "no change wrt" in str(report)


@patch("lb.nightly.functions.gitlab_helpers.gitlabProjectExists", return_value=True)
@patch("lb.nightly.functions.checkout.Repo")
def test_no_change_with_branch(repoMock, _):
    project, report, _ = setup_mock_for_archive(repoMock)

    # no previous tag but branch with same data
    repoMock().create_remote().fetch.side_effect.bad_names = ["test-slot/99"]
    repoMock().create_head().commit.tree.hexsha = report.tree
    repoMock.reset_mock()

    try:
        report = update_nightly_git_archive(project, report)
    finally:
        print(repoMock.mock_calls)

    repoMock().head.reset.assert_any_call(repoMock().create_head().commit)
    assert "previous tag found" in str(report)
