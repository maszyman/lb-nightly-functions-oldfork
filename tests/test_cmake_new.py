import os
import subprocess
import pytest
import elasticsearch
from subprocess import run
from shlex import quote
from unittest.mock import patch
import datetime
from lb.nightly.functions.build import cmake_new
from lb.nightly.configuration import Project, Slot
from .test_checkout import working_directory


@pytest.mark.parametrize("project", (Project("Gaudi", "master"),))
def test_prepare_cache(tmp_path, project):
    with working_directory(tmp_path):
        build = cmake_new(project)
        cache_entries = {
            "CMAKE_USE_CCACHE": True,
        }
        build._prepare_cache(cache_entries=cache_entries)
        cache = """set(CMAKE_USE_CCACHE "True" CACHE STRING "override")
set(CMAKE_C_COMPILER_LAUNCHER ccache CACHE STRING "override")
set(CMAKE_CXX_COMPILER_LAUNCHER ccache CACHE STRING "override")
set(CMAKE_RULE_LAUNCH_COMPILE "lb-wrapcmd <CMAKE_CURRENT_BINARY_DIR> <TARGET_NAME>"
    CACHE STRING "override")
set(CMAKE_RULE_LAUNCH_LINK "lb-wrapcmd <CMAKE_CURRENT_BINARY_DIR> <TARGET_NAME>"
    CACHE STRING "override")
set(CMAKE_RULE_LAUNCH_CUSTOM "lb-wrapcmd <CMAKE_CURRENT_BINARY_DIR> <TARGET_NAME>"
    CACHE STRING "override")
"""
        cache_file = build._cache_preload_file()
        assert os.path.exists(cache_file)
        with open(cache_file) as f:
            assert cache == f.read()


@pytest.mark.parametrize("project", (Project("Gaudi", "master"),))
def test_env(project):
    env = {"key": "value"}
    build = cmake_new(project)
    buildenv = build._env(env=env)
    assert "key" in buildenv and "value" == buildenv["key"]


@pytest.mark.parametrize("project", (Project("Gaudi", "master"),))
def test_strip_kwargs(project):
    build = cmake_new(project)
    os.environ["BINARY_TAG"] = "x86_64-centos7-gcc9-opt"
    with patch.object(elasticsearch.Elasticsearch, "ping", return_value=False):
        build._run(["pwd"])
    for arg in cmake_new.SPECIAL_ARGS:
        assert arg not in build._env()


@pytest.mark.parametrize("project", (Project("Gaudi", "master"),))
def test_run(project):
    build = cmake_new(project)
    cmd = ["echo", "Hello World!"]
    cmd_kwargs = {"step": "configure"}
    import elasticsearch

    with patch.object(elasticsearch.Elasticsearch, "ping", return_value=False):
        build._run(cmd, **cmd_kwargs)

    assert "running echo Hello World!" in str(build.reports["configure"])
    assert "command exited with code 0" in str(build.reports["configure"])
    assert isinstance(build.reports["configure"].returncode, int)
    assert build.reports["configure"].returncode >= 0
    assert build.reports["configure"].command == " ".join(quote(a) for a in cmd)


@patch(
    "lb.nightly.functions.build.datetime",
    **{"now.return_value": datetime.datetime(2020, 7, 17, 15, 34, 25, 854889)},
)
def test_started_completed(date_mock):
    build = cmake_new(Project("Gaudi", "master"))
    cmd = ["echo", "'Hello World!'"]
    with patch.object(elasticsearch.Elasticsearch, "ping", return_value=False):
        build._run(cmd)
    assert "Start: 2020-07-17T15:34:25.854889" in build.reports["script"].stdout
    assert "End: 2020-07-17T15:34:25.854889" in build.reports["script"].stdout


@patch(
    "lb.nightly.functions.build.find_path",
    **{
        "return_value": os.path.join(
            "lcg-toolchains",
            "LCG_97a",
            "x86_64-centos7-gcc9-opt.cmake",
        )
    },
)
@patch("lb.nightly.functions.build.run", **{"return_value.returncode": 0})
def test_lcg_slot(run, path_mock, tmp_path):
    gaudi = Project("Gaudi", "master")
    lcg = Project("LCG", "97a")
    slot = Slot("my-slot", projects=[gaudi, lcg])
    with patch.object(elasticsearch.Elasticsearch, "ping", return_value=False):
        build = cmake_new(gaudi)
        with working_directory(tmp_path):
            env = {}
            env["BINARY_TAG"] = "x86_64-centos7-gcc9-opt"
            build.configure(env=env)
            assert (
                f"-DCMAKE_TOOLCHAIN_FILE={path_mock.return_value}"
                in build.reports["configure"].command
            )


@patch(
    "lb.nightly.functions.build.find_path",
    **{
        "return_value": os.path.join(
            "lcg-toolchains",
            "LCG_97a",
            "x86_64-centos7-gcc9-opt.cmake",
        )
    },
)
@patch("lb.nightly.functions.build.run", **{"return_value.returncode": 0})
def test_lcg_env(run, path_mock, tmp_path):
    gaudi = Project("Gaudi", "master")
    build = cmake_new(gaudi)
    with working_directory(tmp_path):
        env = {}
        env["LCG_VERSION"] = "97a"
        env["BINARY_TAG"] = "x86_64-centos7-gcc9-opt"
        with patch.object(elasticsearch.Elasticsearch, "ping", return_value=False):
            build.configure(env=env)
        assert (
            f"-DCMAKE_TOOLCHAIN_FILE={path_mock.return_value}"
            in build.reports["configure"].command
        )


def test_no_lcg():
    with pytest.raises(EnvironmentError):
        gaudi = Project("Gaudi", "master")
        build = cmake_new(gaudi)
        build.configure(env={})


@patch(
    "lb.nightly.functions.build.find_path",
    **{"return_value": None},
)
def test_no_toolchain(path_mock):
    with pytest.raises(FileNotFoundError):
        gaudi = Project("Gaudi", "master")
        env = {}
        env["LCG_VERSION"] = "97a"
        env["BINARY_TAG"] = "x86_64-centos7-gcc9-opt"
        build = cmake_new(gaudi)
        build.configure(env=env)


@pytest.mark.parametrize("project", (Project("Gaudi", "master"),))
@patch(
    "lb.nightly.functions.build.datetime",
    **{"now.return_value": datetime.datetime(2020, 7, 17, 15, 34, 25, 854889)},
)
@patch(
    "lb.nightly.functions.build.find_path",
    **{
        "return_value": os.path.join(
            "lcg-toolchains",
            "LCG_97a",
            "x86_64-centos7-gcc9-opt.cmake",
        )
    },
)
@patch("lb.nightly.functions.build.run", **{"return_value.returncode": 0})
def test_config_log(run, find_path_mock, date_mock, tmp_path, project):
    build = cmake_new(project)
    env = {}
    env["LCG_VERSION"] = "97a"
    env["BINARY_TAG"] = "x86_64-centos7-gcc9-opt"
    with working_directory(tmp_path):
        with patch.object(elasticsearch.Elasticsearch, "ping", return_value=False):
            build.configure(env=env)
        assert os.path.exists(
            os.path.join(
                project.baseDir,
                "build",
                "configure",
                "{:%s%f000}-build.log".format(
                    datetime.datetime(2020, 7, 17, 15, 34, 25, 854889)
                ),
            )
        )


@patch(
    "lb.nightly.functions.build.find_path",
    **{
        "return_value": os.path.join(
            "lcg-toolchains",
            "LCG_97a",
            "x86_64-centos7-gcc9-opt.cmake",
        )
    },
)
@patch("lb.nightly.functions.build.run", **{"return_value.returncode": 0})
def test_build(run, path_mock, tmp_path):
    gaudi = Project("Gaudi", "master")
    with working_directory(tmp_path):
        env = {}
        env["LCG_VERSION"] = "97a"
        env["BINARY_TAG"] = "x86_64-centos7-gcc9-opt"
        with patch.object(elasticsearch.Elasticsearch, "ping", return_value=False):
            reps = cmake_new(gaudi).build(env=env)
        assert (
            f"-DCMAKE_TOOLCHAIN_FILE={path_mock.return_value}"
            in reps["configure"].command
        )


@patch(
    "lb.nightly.functions.build.find_path",
    **{
        "return_value": os.path.join(
            "lcg-toolchains",
            "LCG_97a",
            "x86_64-centos7-gcc9-opt.cmake",
        )
    },
)
@patch(
    "lb.nightly.functions.build.run",
)
def test_configuration_failed(run, toolchain_mock):
    run.return_value.returncode = 1
    env = {}
    env["LCG_VERSION"] = "97a"
    env["BINARY_TAG"] = "x86_64-centos7-gcc9-opt"
    with patch.object(elasticsearch.Elasticsearch, "ping", return_value=False):
        result = cmake_new(Project("Gaudi", "master")).build(env=env)
    assert result["configure"].returncode == 1
    assert not hasattr(result["build"], "returncode")


@patch(
    "lb.nightly.functions.build.find_path",
    **{
        "return_value": os.path.join(
            "lcg-toolchains",
            "LCG_97a",
            "x86_64-centos7-gcc9-opt.cmake",
        )
    },
)
@patch(
    "lb.nightly.functions.build.run",
)
@patch(
    "lb.nightly.functions.build.singularity_run",
)
def test_build_ok(singularity_run, run, toolchain_mock):
    run.return_value.returncode = 0
    singularity_run.return_value.returncode = 0
    env = {}
    env["LCG_VERSION"] = "97a"
    env["BINARY_TAG"] = "x86_64-centos7-gcc9-opt"
    with patch.object(elasticsearch.Elasticsearch, "ping", return_value=False):
        result = cmake_new(Project("Gaudi", "master")).build(env=env)
    for step in ["configure", "build", "install"]:
        assert result[step].returncode == 0
    assert (
        "cmake -S Gaudi/ -B Gaudi/build -G Ninja -C Gaudi/cache_preload.cmake -DCMAKE_TOOLCHAIN_FILE=lcg-toolchains/LCG_97a/x86_64-centos7-gcc9-opt.cmake"
        in result["configure"].stdout
    )
    assert "cmake --build Gaudi/build" in result["build"].stdout
    assert (
        "cmake --install Gaudi/build --prefix Gaudi/InstallArea/x86_64-centos7-gcc9-opt"
        in result["install"].stdout
    )
