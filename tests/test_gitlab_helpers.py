###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from lb.nightly.configuration import Project, Slot
from lb.nightly.functions.common import Report
import datetime
from unittest.mock import patch, call, MagicMock
import pytest

from lb.nightly.functions import gitlab_helpers


@patch("gitlab.Gitlab")
def test_getMRTitle(gitlabMock):
    # set the title to be returned by the mock
    gitlabMock().projects.get().mergerequests.get().title = "This is a MR title"

    report = Report()
    title = gitlab_helpers.getMRTitle("gaudi/Gaudi", 213, "a token", report)

    gitlabMock.assert_has_calls(
        [
            call("https://gitlab.cern.ch/", "a token"),
            call().projects.get("gaudi/Gaudi"),
            call().projects.get().mergerequests.get(213),
        ]
    )
    assert title == "This is a MR title"

    assert "merge request 213 in project gaudi/Gaudi" in str(report)


@patch("gitlab.Gitlab")
def test_getMRTitle_error(gitlabMock):
    # set the title to be returned by the mock
    gitlabMock().projects.get.side_effect = ValueError("bad project id")

    report = Report()
    with pytest.raises(ValueError):
        gitlab_helpers.getMRTitle("gaudi/Gaudi", 213, "a token", report)

    assert "bad project id" in str(report)


def test_getMRTitle_no_token(monkeypatch):
    monkeypatch.delenv("GITLAB_TOKEN", raising=False)
    report = Report()
    assert gitlab_helpers.getMRTitle("gaudi/Gaudi", 213, None, report) == ""


@patch("gitlab.Gitlab")
def test_getMRTitle_env_token(gitlabMock, monkeypatch):
    # set the title to be returned by the mock
    gitlabMock().projects.get().mergerequests.get().title = "This is a MR title"
    monkeypatch.setenv("GITLAB_TOKEN", "env token")

    report = Report()
    title = gitlab_helpers.getMRTitle("gaudi/Gaudi", 213, None, report)

    gitlabMock.assert_has_calls(
        [
            call("https://gitlab.cern.ch/", "env token"),
            call().projects.get("gaudi/Gaudi"),
            call().projects.get().mergerequests.get(213),
        ]
    )
    assert title == "This is a MR title"

    assert "merge request 213 in project gaudi/Gaudi" in str(report)


@patch("gitlab.Gitlab")
def test_getMRTitle_env_token_priority(gitlabMock, monkeypatch):
    # set the title to be returned by the mock
    gitlabMock().projects.get().mergerequests.get().title = "This is a MR title"
    monkeypatch.setenv("GITLAB_TOKEN", "env token")

    report = Report()
    title = gitlab_helpers.getMRTitle("gaudi/Gaudi", 213, "explicit token", report)

    gitlabMock.assert_has_calls(
        [
            call("https://gitlab.cern.ch/", "explicit token"),
            call().projects.get("gaudi/Gaudi"),
            call().projects.get().mergerequests.get(213),
        ]
    )
    assert title == "This is a MR title"

    assert "merge request 213 in project gaudi/Gaudi" in str(report)


@patch("urllib.request.urlopen")
def test_gitlabProjectExists(mock):
    mock().getcode.return_value = 200
    mock.reset_mock()

    report = Report()
    assert gitlab_helpers.gitlabProjectExists("some/Project", report)
    assert "probing some/Project in Gitlab" in str(report)

    mock.called_once_with(
        "https://gitlab.cern.ch/some/Project.git/info/refs?service=git-upload-pack"
    )


@patch("urllib.request.urlopen")
def test_gitlabProjectExists_false(mock):
    mock().getcode.return_value = 400
    mock.reset_mock()

    report = Report()
    assert not gitlab_helpers.gitlabProjectExists("some/Project", report)
    assert "probing some/Project in Gitlab" in str(report)

    mock.called_once_with(
        "https://gitlab.cern.ch/some/Project.git/info/refs?service=git-upload-pack"
    )


@patch("urllib.request.urlopen")
def test_gitlabProjectExists_error(mock):
    mock().getcode.side_effect = RuntimeError
    mock.reset_mock()

    report = Report()
    assert not gitlab_helpers.gitlabProjectExists("some/Project", report)
    assert "probing some/Project in Gitlab" in str(report)

    mock.called_once_with(
        "https://gitlab.cern.ch/some/Project.git/info/refs?service=git-upload-pack"
    )


@patch(
    "lb.nightly.functions.gitlab_helpers.datetime",
    **{"now.return_value": datetime.datetime(2020, 6, 7, 19, 0)},
)
@patch("gitlab.Gitlab")
def test_postToMergeRequest_basic(gitlabMock, dateMock):
    mreq = gitlabMock().projects.get().mergerequests.get()
    gitlabMock.reset_mock()

    report = Report()
    gitlab_helpers.postToMergeRequest(
        "gaudi/Gaudi",
        1234,
        "some message",
        new_comment=False,
        token="a token",
        logger=report,
    )

    print(gitlabMock.mock_calls)

    mreq.notes.create.assert_called_with(
        {"body": "- __[2020-06-07 19:00]__ some message"}
    )


@patch(
    "lb.nightly.functions.gitlab_helpers.datetime",
    **{"now.return_value": datetime.datetime(2020, 6, 7, 19, 0)},
)
@patch("gitlab.Gitlab")
def test_postToMergeRequest_new_comment(gitlabMock, dateMock):
    mreq = gitlabMock().projects.get().mergerequests.get()
    gitlabMock.reset_mock()

    report = Report()
    gitlab_helpers.postToMergeRequest(
        "gaudi/Gaudi",
        1234,
        "some message",
        new_comment=True,
        token="a token",
        logger=report,
    )

    print(gitlabMock.mock_calls)

    mreq.notes.create.assert_called_with({"body": "some message"})


@patch(
    "lb.nightly.functions.gitlab_helpers.datetime",
    **{"now.return_value": datetime.datetime(2020, 6, 7, 19, 0)},
)
@patch("gitlab.Gitlab")
def test_postToMergeRequest_discussion(gitlabMock, dateMock):
    gitlabMock().user.id = "someuser"
    mreq = gitlabMock().projects.get().mergerequests.get()
    mreq.notes.list.return_value = [
        MagicMock(system=False),  # ignored (setting wrong id in a few lines)
        MagicMock(system=False, body="first note"),
        MagicMock(system=True),  # ignored
        MagicMock(system=False, body="good note"),
        MagicMock(system=False, type="DiscussionNote"),  # ignored
    ]
    for i, n in enumerate(mreq.notes.list.return_value):
        gitlabMock.attach_mock(n, f"_note_{i}")
        if i > 0:
            n.author.__getitem__.return_value = "someuser"
    mreq.notes.list.return_value.reverse()  # notes are retrieved from more recent to oldest
    gitlabMock.reset_mock()

    report = Report()
    gitlab_helpers.postToMergeRequest(
        "gaudi/Gaudi",
        1234,
        "some message",
        new_comment=False,
        token="a token",
        logger=report,
    )

    print(gitlabMock.mock_calls)

    gitlabMock._note_3.save.assert_called_once()

    assert gitlabMock._note_3.body == "good note\n- __[2020-06-07 19:00]__ some message"


@patch(
    "lb.nightly.functions.gitlab_helpers.datetime",
    **{"now.return_value": datetime.datetime(2020, 6, 7, 19, 0)},
)
@patch("gitlab.Gitlab")
def test_postToMergeRequest_error(gitlabMock, dateMock):
    gitlabMock().projects.get.side_effect = RuntimeError("got a problem here!")
    gitlabMock.reset_mock()

    report = Report()
    with pytest.raises(RuntimeError):
        gitlab_helpers.postToMergeRequest(
            "gaudi/Gaudi",
            1234,
            "some message",
            new_comment=True,
            token="a token",
            logger=report,
        )

    assert "got a problem here!" in str(report)


@patch("lb.nightly.functions.gitlab_helpers.postToMergeRequest")
@pytest.mark.parametrize(
    "project",
    [
        None,
        Project("Gaudi", "HEAD"),
        Slot("some-slot", projects=[Project("Gaudi", "HEAD")]).Gaudi,
        Slot("some-slot", build_id=123, projects=[Project("Gaudi", "HEAD")]).Gaudi,
    ],
)
def test_notifyMergeRequest_noop(postMock, project, monkeypatch):
    try:
        assert project.slot.build_id  # if the project is OK, turn off by env
        monkeypatch.setenv("NO_UPDATE_MR", "1")
    except Exception:
        monkeypatch.delenv("NO_UPDATE_MR", raising=False)
    report = Report()

    gitlab_helpers.notifyMergeRequest(
        project,
        "gaudi/Gaudi",
        1234,
        True,
        token="a token",
        logger=report,
    )

    postMock.assert_not_called()


@patch("lb.nightly.functions.gitlab_helpers.postToMergeRequest")
def test_notifyMergeRequest_no_token(postMock, monkeypatch):
    monkeypatch.delenv("GITLAB_TOKEN", raising=False)
    report = Report()

    project = Slot("some-slot", build_id=123, projects=[Project("Gaudi", "HEAD")]).Gaudi
    gitlab_helpers.notifyMergeRequest(
        project,
        "gaudi/Gaudi",
        1234,
        True,
        token=None,
        logger=report,
    )

    postMock.assert_not_called()
    assert "cannot post comment" in str(report)


@patch("lb.nightly.functions.gitlab_helpers.postToMergeRequest")
@pytest.mark.parametrize(
    "success",
    [True, False],
)
def test_notifyMergeRequest(postMock, success):
    report = Report()

    project = Slot("some-slot", build_id=123, projects=[Project("Gaudi", "HEAD")]).Gaudi
    gitlab_helpers.notifyMergeRequest(
        project,
        "gaudi/Gaudi",
        99,
        success,
        token="a token",
        logger=report,
    )

    postMock.assert_called_once()

    # This version requires Python 3.8
    # message = postMock.call_args.args[2]
    message = postMock.call_args[0][2]

    expected = "Validation started with" if success else "Automatic merge failed"
    assert expected in message
    assert "[some-slot#123](" in message
