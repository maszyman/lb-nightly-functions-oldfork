from lb.nightly.configuration import Slot, Project
from lb.nightly.functions.common import get_build_method
from lb.nightly.functions.build import cmake, cmt


def test_get_build_method():

    p = Project("a", "v1r0", env=["proj=a"])
    slot = Slot(
        "test",
        projects=[p],
    )
    assert get_build_method(p) == cmake
    p = Project("a", "v1r0", build_tool="cmt")
    slot = Slot(
        "test",
        projects=[p],
    )
    assert get_build_method(p) == cmt
    p = Project("a", "v1r0")
    slot = Slot("test", projects=[p], build_tool="cmt")
    assert get_build_method(p) == cmt
    p = Project("a", "v1r0", build_tool="cmt")
    slot = Slot("test", projects=[p], build_tool="cmake")
    assert get_build_method(p) == cmt
