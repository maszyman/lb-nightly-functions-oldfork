###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from lb.nightly.configuration import Project, Package
from lb.nightly.functions.common import Report
from unittest.mock import patch, call
import pytest

from lb.nightly.functions.checkout import notify_gitlab


@patch("lb.nightly.functions.gitlab_helpers.notifyMergeRequest")
def test_bad_report(mockNotify):
    report = Report()
    assert "summary of merge results missing in report" not in str(report)

    notify_gitlab(Project("Gaudi", "master"), report)

    mockNotify.assert_not_called()
    assert "summary of merge results missing in report" in str(report)


@patch("lb.nightly.functions.gitlab_helpers.notifyMergeRequest")
def test_basic_notification(mockNotify):
    report = Report()
    report.merges = {"success": [1, 2], "failure": [3]}
    project = Project("Gaudi", "master")

    notify_gitlab(project, report, "some token")

    mockNotify.assert_has_calls(
        [
            call(project, report.gitlab_id, 1, True, "some token", report),
            call(project, report.gitlab_id, 2, True, "some token", report),
            call(project, report.gitlab_id, 3, False, "some token", report),
        ]
    )


@patch("lb.nightly.functions.gitlab_helpers.notifyMergeRequest")
def test_basic_no_notification(mockNotify):
    report = Report()
    report.merges = {"success": [], "failure": []}
    project = Project("Gaudi", "master")

    notify_gitlab(project, report, "some token")

    mockNotify.assert_not_called()


@patch("lb.nightly.functions.gitlab_helpers.notifyMergeRequest")
def test_project_id(mockNotify):
    report = Report()
    report.merges = {"success": [1], "failure": []}

    project = Project("Gaudi", "master")
    notify_gitlab(project, report, "some token")
    mockNotify.called_once_with(project, "gaudi/Gaudi", 1, True, "some token", report)

    mockNotify.reset_mock()
    project = Project("Moore", "master")
    notify_gitlab(project, report, "some token")
    mockNotify.called_once_with(project, "lhcb/Moore", 1, True, "some token", report)

    mockNotify.reset_mock()
    project = Package("PRConfig", "master")
    notify_gitlab(project, report, "some token")
    mockNotify.called_once_with(
        project, "lhcb-datapkg/PRConfig", 1, True, "some token", report
    )

    mockNotify.reset_mock()
    project = Package("Hat/Package", "master")
    notify_gitlab(project, report, "some token")
    mockNotify.called_once_with(
        project, "lhcb-datapkg/Hat/Package", 1, True, "some token", report
    )

    mockNotify.reset_mock()
    project = Project(
        "Gaudi",
        "master",
        checkout_opts={"url": "https://gitlab.cern.ch/user/MyFork.git"},
    )
    notify_gitlab(project, report, "some token")
    mockNotify.called_once_with(project, "user/MyFork", 1, True, "some token", report)
