import os

from lb.nightly.configuration import Project
from lb.nightly.functions.build import cmake, cmake_new, cmake_old, is_new_cmake_style
from .test_checkout import working_directory


def test_recognise_cmt(tmp_path):
    with working_directory(tmp_path):
        assert not is_new_cmake_style(tmp_path)


def test_custom_toolchain(tmp_path):
    with working_directory(tmp_path):
        with open(os.path.join(tmp_path, "toolchain.cmake"), "wb"):
            pass
        assert not is_new_cmake_style(tmp_path)


def test_find_package(tmp_path):
    with working_directory(tmp_path):
        with open(os.path.join(tmp_path, "CMakeLists.txt"), "wb"):
            pass
        assert is_new_cmake_style(tmp_path)
        with open(os.path.join(tmp_path, "CMakeLists.txt"), "wb") as f:
            f.write(b"find_package(GaudiProject)")
        assert not is_new_cmake_style(tmp_path)


def test_project_cmake_new(tmp_path):
    p = Project("Gaudi", "master")
    with working_directory(tmp_path):
        os.makedirs(p.baseDir)
        with open(os.path.join(tmp_path, p.baseDir, "CMakeLists.txt"), "wb"):
            pass
        build = cmake(p)
        assert isinstance(build, cmake_new)


def test_project_cmake_old(tmp_path):
    p = Project("Gaudi", "master")
    with working_directory(tmp_path):
        os.makedirs(p.baseDir)
        with open(os.path.join(tmp_path, "CMakeLists.txt"), "wb") as f:
            f.write(b"find_package(GaudiProject)")
        build = cmake(p)
        assert isinstance(build, cmake_old)


def test_project_cmake_old(tmp_path):
    p = Project("Gaudi", "master")
    with working_directory(tmp_path):
        os.makedirs(p.baseDir)
        with open(os.path.join(tmp_path, "toolchain.cmake"), "wb"):
            pass
        build = cmake(p)
        assert isinstance(build, cmake_old)
